<?php

function rma_pending_all() {
  // content variable that will be returned for display
  $page_content = '';



$header = array('#','RMA No', 'Product Name', 'Warranty', 'Problem', 'Company',  'Repair Status', 'TAT');
  $rows = array();
//  $noyes = array('No', 'Yes');
// {node}
  $query = "SELECT * FROM {rma_workspace}";
  $query_result =  db_query($query);
  foreach ($query_result as $key=>$record) {
// print_r ($record);
//    $page_content .=  l($record->RMA_No, 'node/'. $record->RMA_No) .'<br />';
	$rows[] = array(
		'#'=> $key, 
		'RMA No' => l($record->rma_no, 'rma_repair/'.($record->rma_no)), 
		'Product Name'=> $record->product_name, 
		'Warranty'=> $record->warranty_type, 
		'Problem'=> substr($record->problem_description,0,20), 
		'Company'=> substr($record->order_by,0,20), 
		'Status'=> $record->repair_status, 
		'TAT' => $record->tat);
  }
$page_content .= ( theme('table',   array('header' => $header, 'rows' => $rows)));
$page_content .= theme ('pager',array(),10,0);
	

  return $page_content;
	}	// rma_pending_all end
?>

