<?php
function rma_request_all() {

$page_content = '';
return (drupal_get_form('rma_request_form'));

}		// rma_request_all end


function rma_request_form($form, &$form_state) {

	$form['Product_Name'] = array(
	'#type' => 'textfield',
	'#title' => t('Product Name (SKU)'),
	'#default_value' => '',
	'#size' => 30,
	'#maxlength' => 30,
//	'#description' => t('Product Name'),
	);

	$form['Barcode'] = array(
	'#type' => 'textfield',
	'#title' => t('Serial Number'),
	'#default_value' => '',
	'#size' => 30,
	'#maxlength' => 30,
	'#required' => TRUE,
//	'#description' => t('Product Name'),
//	'#element_validate' => array('rma_barcode_validator'),	// uCart/SAP lookup in future
	);

	$form['Problem_Description'] = array(
	'#type' => 'textfield',
	'#title' => t('Problem Description'),
	'#default_value' => '',
	'#size' => 30,
	'#maxlength' => 30,
	'#required' => TRUE,
//	'#description' => t('Product Name'),
	);

 	$form['submit'] = array(
	'#type' => 'submit',
	'#value' => 'Add',
	);
	return ($form);
	}	//	request_form end


function rma_request_form_validate($form, &$form_state) {
	if(strlen($form_state['values']['Problem_Description'])<5)	form_set_error('Problem_Description', t('Problem description is too short. Please provide more detailed.'));
	if(strlen($form_state['values']['Barcode'])<5)	form_set_error('Barcode', t('Serial Number is invalid. Please check again.'));
}	// validate end

function rma_request_form_submit($form, &$form_state) {

	global $user;	$Order_BY = $user->mail;
	db_insert('rma_workspace') -> fields(array(
		'Product_Name' => $form_state['values']['Product_Name'],
		'Barcode' => $form_state['values']['Barcode'],
		'Problem_Description' => $form_state['values']['Problem_Description'],
		'Order_BY' => $Order_BY,
		'Order_DT' => REQUEST_TIME,
		)) ->execute();
	drupal_set_message(t('New RMA case has ben created.'));
	drupal_goto('rma_pending/');


}

?>

